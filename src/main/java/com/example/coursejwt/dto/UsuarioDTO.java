package com.example.coursejwt.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class UsuarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String uid;
    private String name;
    private String last_name;
    private String role;
    private String country;

}
