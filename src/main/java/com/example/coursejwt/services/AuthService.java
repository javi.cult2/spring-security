package com.example.coursejwt.services;

import com.example.coursejwt.dto.JWTResponse;
import com.example.coursejwt.dto.UsuarioDTO;
import com.example.coursejwt.security.JWTIO;
import com.example.coursejwt.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AuthService {

    @Autowired
    private JWTIO jwtio;

    @Autowired
    private DateUtils dateUtils;

    @Value("${jms.jwt.token.expires-in}")
    private int EXPIRES_IN;

    public JWTResponse login(String clientId, String clientSecret) {

        UUID uuid = UUID.randomUUID();

        UsuarioDTO user =UsuarioDTO.builder()
                .name("Javi")
                .last_name("Cult")
                .role("Admin")
                .country("Mexico")
                .uid(uuid.toString())
                .build();

        JWTResponse jwt = JWTResponse.builder()
                .tokenType("bearer")
                .accessToken(jwtio.generateToken(user))
                .issuedAt(dateUtils.getDateMillis() + "")
                .clientId(clientId)
                .expiresIn(EXPIRES_IN)
                .build();

        return jwt;
    }
}
